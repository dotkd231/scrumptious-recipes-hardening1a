from django.urls import path
from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetail,
    MealPlanUpdate,
    MealPlanDelete,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plan_list"),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="meal_plans_create",
    ),
    path(
        "meal_plans/<int:pk>/",
        MealPlanDetail.as_view(),
        name="meal_plans_detail",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdate.as_view(),
        name="meal_plans_edit",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDelete.as_view(),
        name="meal_plans_delete",
    ),
]
