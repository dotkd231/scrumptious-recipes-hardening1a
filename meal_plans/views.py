from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from meal_plans.models import Meal_Plans
from django.shortcuts import redirect

# Create your views here.


class MealPlanListView(ListView, LoginRequiredMixin):
    model = Meal_Plans
    template_name = "mealplans/list.html"
    fields = "name", "ingredients"

    def get_queryset(self):
        return Meal_Plans.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plans
    template_name = "mealplans/new.html"
    fields = ["name", "date", "owner"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlanDetail(LoginRequiredMixin, DetailView):
    model = Meal_Plans
    template_name = "mealplans/detail.html"

    def get_queryset(self):
        return MealPlanDetail.objects.filter(owner=self.request.user)


class MealPlanUpdate(LoginRequiredMixin, UpdateView):
    model = Meal_Plans
    template_name = "mealplans/edit.html"
    fields = ["name", "date", "owner"]

    def get_queryset(self):
        return Meal_Plans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_detail", args=[self.object.id])


class MealPlanDelete(LoginRequiredMixin, DeleteView):
    model = Meal_Plans
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("meal_plans_detail")

    def get_queryset(self):
        return Meal_Plans.objects.filter(owner=self.request.user)
